import React from 'react';

class App extends React.Component {
  constructor(props) {
    super(props);

    // Initialize Application State
    this.state = {}

    // Bind methods to 'this'
    this.campaignMove = this.campaignMove.bind(this);
    this.getCampaigns = this.getCampaigns.bind(this);
    this.getUser = this.getUser.bind(this);
    this.logout = this.logout.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
    this.handleCampaignSubmit = this.handleCampaignSubmit.bind(this);
  }

  // When the App loads, get current logged in User, if logged in
  componentWillMount() {
    this.getUser();
  }

  campaignMove(e, dir, current) {
    // Flipping campaign priorities between two adjacent campaigns,
    // where current is the index of one campaign, and dir, is the direction it is moving in (-1, 1)
    this.setState((ps) => {
      [ps.campaigns[current].campaignPriority, ps.campaigns[current+dir].campaignPriority] =
        [ps.campaigns[current+dir].campaignPriority, ps.campaigns[current].campaignPriority];
      [ps.campaigns[current], ps.campaigns[current+dir]] = [ps.campaigns[current+dir], ps.campaigns[current]];

      return(ps);
    })
  }

  getCampaigns() {
    fetch('/campaign/all', {
      method: 'GET',
    })
    .then((res) => (res.json()))
    .catch((err) => (console.error(err)))
    .then((res) => {
      if (res.Error) {
        console.error(res.Error);
      } else {
        this.setState({campaigns: res});
      }
    });
  }

  getUser() {
    // Get the token cookie
    let token = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");

    if (token) {
      // Save token to the state
      this.setState({
        token: token,
      });

      // Hit Campaign Endpoint to get the appropriate Campaign Image
      fetch('/campaign', {
        method: 'GET',
      })
      .then((res) => (res.json()))
      .catch((err) => (console.error(err)))
      .then((res) => {
        this.setState({
          username: res.username,
          img: `assets/${res.imgPath}`,
        });
        this.getCampaigns();
      });
    }
  }

  logout() {
    // Delete the token cookie
    document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

    // Clear state of any User data
    this.setState({
      username: '',
      token: '',
      img: '',
    });
  }

  handleChange(event) {
    // When form fields change, update their corresponding state fields
    this.setState({[event.target.id]: event.target.value});
  }

  handleLoginSubmit(event) {
    // Add form fields to an object to be sent with a request
    let body = new URLSearchParams();
    body.append('username', this.state.formUsername);
    body.append('password', this.state.formPassword);

    // Hit the Login Endpoint with credentials to login.
    fetch('/login', {
      method: 'POST',
      body: body,
      credentials: 'include',
    })
    .then((res) => (res.json()))
    .catch((err) => (console.error(err)))
    .then((res) => {
      if (res.Error) {
        // If credentials were invalid, set state to flash message on front end
        this.setState({failure: true});
      } else {
        // If credentials were valid, save the token to state and cookies,
        // Call the getUser method, and hide the login modal
        this.setState({token: res.token, failure: false});
        document.cookie = `token=${res.token}`;
        this.getUser();
        $('#loginModal').modal('hide');
      }
    });

    // Clear the form after the form has been submitted
    this.setState({
      formUsername: '',
      formPassword: '',
    })

    event.preventDefault();
  }

  handleCampaignSubmit(event) {
    // Add form fields to an object to be sent with a request
    let body = new URLSearchParams();
    body.append('campaigns', JSON.stringify(this.state.campaigns));

    // Hit the Login Endpoint with credentials to login.
    fetch('/campaign/all', {
      method: 'POST',
      body: body,
      credentials: 'include',
    })
    .then((res) => (res.json()))
    .catch((err) => (console.error(err)))
    .then((res) => {
      if (res.Error) {
        // If credentials were invalid, set state to flash message on front end
        this.setState({failure: true});
      } else {
        // Now that campaign priority is updated, get new user details.
        this.getUser();
        $('#campaignModal').modal('hide');
      }
    });

    // Clear the form after the form has been submitted
    this.setState({
      formUsername: '',
      formPassword: '',
    })

    event.preventDefault();
  }

  render() {
    return (
      <div>
        <div className="modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="loginModalLabel">Login</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form onSubmit={this.handleLoginSubmit}>
                <div className="modal-body">
                  <div className="form-group">
                    <label for="username">Username</label>
                    <input type="text" className="form-control" id="formUsername" placeholder="Enter Username" value={this.state.formUsername} onChange={this.handleChange}/>
                  </div>
                  <div className="form-group">
                    <label for="password">Password</label>
                    <input type="password" className="form-control" id="formPassword" placeholder="Enter Password" value={this.state.formPassword} onChange={this.handleChange}/>
                  </div>
                  {this.state.failure &&
                    <div className="alert alert-danger form-group" role="alert">
                      <p>
                        Invalid Username or Password
                      </p>
                    </div>
                  }
                </div>
                <div className="modal-footer">
                  <button type="submit" className="btn btn-primary">Login</button>
                </div>

              </form>
            </div>
          </div>
        </div>

        {this.state.campaigns &&
          <div className="modal" id="campaignModal" tabindex="-1" role="dialog" aria-labelledby="campaignModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="campaignModalLabel">Campaigns</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form onSubmit={this.handleCampaignSubmit}>
                  <div className="modal-body table-responsive">
                    <table className="table table-striped table-bordered">
                      <thead>
                        <tr>
                          {this.state.campaigns.map((e) => (
                            <th className="text-center" scope="col">{(e.geoDesc || e.industryDesc || e.companySizeDesc || 'N/A')}</th>
                          ))}
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          {this.state.campaigns.map((e) => (
                            <td className="align-middle">
                              <img className="img-fluid img-thumbnail" src={`/assets/${e.campaignImg}`} alt={e.campaignImg}/>
                            </td>
                          ))}
                        </tr>
                        <tr>
                          {this.state.campaigns.map((e, i, a) => (
                            <td className="align-middle">
                              <ul className="list-inline text-center text-nowrap">
                                {i != 0 && i != a.length-1 &&
                                  <li className="list-inline-item h4"><i onClick={(e) => (this.campaignMove(e, -1, i))} className="fas fa-caret-left"></i></li>
                                }
                                <li className="list-inline-item h2">{e.campaignPriority}</li>
                                {i < a.length-2 &&
                                  <li className="list-inline-item h4"><i onClick={(e) => (this.campaignMove(e, 1, i))} className="fas fa-caret-right"></i></li>
                                }
                              </ul>
                            </td>
                          ))}
                        </tr>
                      </tbody>
                    </table>
                    {this.state.failure &&
                      <div className="alert alert-danger form-group" role="alert">
                        <p>
                          Invalid Username or Password
                        </p>
                      </div>
                    }
                  </div>
                  <div className="modal-footer">
                    <button type="submit" className="btn btn-primary">Save Changes</button>
                  </div>

                </form>
              </div>
            </div>
          </div>
        }

        <nav className="navbar navbar-expand-lg navbar-light bg-light d-flex justify-content-between">
          <a className="navbar-brand mb-0 h1" href="#">Proof</a>
          {this.state.username &&
            <span className="navbar-text">
              Logged in as: <strong>{this.state.username}</strong>
            </span>
          }
          <ul className="navbar-nav mr-0">
            {this.state.username &&
              <li className="nav-item">
                <button className="btn btn-outline-primary my-2 my-sm-0 mr-sm-2" type="submit" onClick={this.logout}>Logout</button>
              </li>
            }
            {this.state.username &&
              <li className="nav-item">
                <button className="btn btn-outline-primary my-2 my-sm-0" type="button" data-toggle="modal" data-target="#campaignModal">
                  <i class="fas fa-cog"/>
                </button>
              </li>
            }
            {!this.state.username &&
              <li className="nav-item">
                <button className="btn btn-outline-primary my-2 my-sm-0" type="button" data-toggle="modal" data-target="#loginModal">
                  Login
                </button>
              </li>
            }
          </ul>
        </nav>
        <div className="container py-md-3">
          <div className="row justify-content-md-center">
            <div className="col-md-8">
              {this.state.username &&
                <div className="row justify-content-md-center">
                  <div className="alert alert-primary alert-dismissible fade show" role="alert">
                    <h4 class="alert-heading">Hey There!</h4>
                    <p>
                      Now that you are logged in, if you would like to change the
                      campaign priorities, click the cog (<i class="fas fa-cog"/>)
                      to right corner.
                    </p>
                    <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <img className="img-fluid img-thumbnail" src={this.state.img}/>
                </div>
              }
              {!this.state.username &&
                <div className="alert alert-primary alert-dismissible fade show" role="alert">
                  <h4 class="alert-heading">Hey There!</h4>
                  <p>
                    It seems you are not logged in! Please log in by clicking the
                    Login button in the top right corner to see more.
                  </p>
                  <hr/>
                  <p>
                    <em>The Username is User[1-10], with the password being Password[1-10], respectively.</em>
                  </p>
                  <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default App;
