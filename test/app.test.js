import assert from 'assert';
import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { JSDOM } from 'jsdom';

import App from '../src/app';

Enzyme.configure({ adapter: new Adapter() })

const Wrapper = Enzyme.shallow(<App />)

describe('App Component', () => {
  describe('Default App State', () => {
    it('Renders alert to login', () => {
      assert.equal(Wrapper.find('.alert.alert-primary').children('h4').text(), 'Hey There!');
    });

    it('Image does not render', () => {
      assert.ok(!Wrapper.exists('img'));
    });

    it('Invalid credentials alert does not render', () => {
      assert.ok(!Wrapper.exists('.alert.alert-danger'));
    });
  });

  describe('Invalid Credentials Entered', () => {
    before(() => (Wrapper.setState({ failure: true })));

    it('Invalid credentials alert renders', () => {
      assert.ok(Wrapper.exists('.alert.alert-danger'));
    });
  });

  describe('Logged In', () => {
    before(() => (Wrapper.setState({ username: 'User3' })))
    it('Image renders', () => {
      assert.ok(Wrapper.exists('img'));
    });

    it('Username in navbar renders', () => {
      assert.equal(Wrapper.find('.navbar-text').text(), 'Logged in as: User3');
    });
  });
});
