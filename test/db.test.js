const assert = require('assert');
const sqlite3 = require('sqlite3').verbose();

const initDB = require('../db/db');

const db = new sqlite3.Database('db.sqlite3');

// Error handler callback for db operations.
const errorHandler = (err) => {
  if (err) console.log(err);
};

// Method to clear out schema
const dropTables = () => {
  db.serialize(() => {
    db.run('DELETE FROM user;');
    db.run('DELETE FROM geo;');
    db.run('DELETE FROM industry;');
    db.run('DELETE FROM companySize;');
    db.run('DELETE FROM campaign;');
  });
}

const verifyRowCount = (table, numRows) => {
  it(`Confirm Table ${table} has ${numRows} row(s)`, (done) => {
    db.get(`SELECT COUNT(*) count FROM ${table}`, (err, d) => {
      if (err) { assert.fail(err); }
      assert.equal(d.count, numRows);
      done();
    });
  });
}


describe('DB Tests', () => {
  after(() => (db.close()));

  describe('Verifying tables are empty', () => {
    before(dropTables);

    verifyRowCount('user', 0);
    verifyRowCount('geo', 0);
    verifyRowCount('industry', 0);
    verifyRowCount('companySize', 0);
    verifyRowCount('campaign', 0);
  });

  describe('Verifying tables are populated after running initDB', () => {
    before((done) => (initDB('assets/Proof_homework.csv', done)));

    verifyRowCount('user', 10);
    verifyRowCount('geo', 6);
    verifyRowCount('industry', 6);
    verifyRowCount('companySize', 4);
    verifyRowCount('campaign', 7);
  });

  describe('Verifying tables are not being over populated from each call of initDB', () => {
    before((done) => {
      initDB('assets/Proof_homework.csv', initDB('assets/Proof_homework.csv', done));
    });

    verifyRowCount('user', 10);
    verifyRowCount('geo', 6);
    verifyRowCount('industry', 6);
    verifyRowCount('companySize', 4);
    verifyRowCount('campaign', 7);
  });
});
