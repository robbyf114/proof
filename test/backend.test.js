const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');

const app = require('../app');

chai.use(chaiHttp);

describe('App', () => {
  let token = ['eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlVzZXIyIiwicGFzc3dvcmQiOiJQYXNzd29yZDIiLCJpYXQiOjE1NDAyNjMxNzYsImV4cCI6MTU0MDI3MDM3Nn0.a4l6zJw8K86Jpo34y2lJRdojjGfEpygZBIhw3oa6p00'];

  let campaigns = [
    {campaignID: 1, campaignPriority: 1, geoDesc: 'Austin', industryDesc: null, companySizeDesc: null, campaignImg: 'Austin.jpg'},
    {campaignID: 2, campaignPriority: 2, geoDesc: 'San Francisco', industryDesc: null, companySizeDesc: null, campaignImg: 'SanFrancisco.jpg'},
    {campaignID: 3, campaignPriority: 3, geoDesc: null, industryDesc: 'Software', companySizeDesc: null, campaignImg: 'Software.jpg'},
    {campaignID: 4, campaignPriority: 4, geoDesc: null, industryDesc: 'Sports', companySizeDesc: null, campaignImg: 'Sports.jpg'},
    {campaignID: 5, campaignPriority: 5, geoDesc: null, industryDesc: null, companySizeDesc: '0-50', campaignImg: 'proof.png'},
    {campaignID: 6, campaignPriority: 6, geoDesc: null, industryDesc: null, companySizeDesc: '100-200', campaignImg: 'smb.jpg'},
    {campaignID: 7, campaignPriority: 7, geoDesc: null, industryDesc: null, companySizeDesc: null, campaignImg: 'shrug.jpg'},
  ]


  describe('/', () => {
    it('Responds with 200', (done) => {
      chai.request(app).get('/').end((err, res) => {
        if (err) { assert.fail(err); }
        assert.equal(res.status, 200);
        done();
      });
    });
  });

  describe('/login', () => {
    it('Responds with 401 for invalid credentials', (done) => {
      chai.request(app).post('/login').type('form')
        .send({username: 'User1', password: 'Password4'}).end((err, res) => {
          if (err) { assert.fail(err); }
          assert.equal(res.status, 401);
          done();
        });
    });

    it('Responds with token for valid credentials', (done) => {
      chai.request(app).post('/login').type('form')
        .send({username: 'User3', password: 'Password3'}).end((err, res) => {
          if (err) { assert.fail(err); }
          token.push(res.body.token);
          assert.ok(res.body.token);
          done();
        });
    });

    it('Responds with token for valid credentials', (done) => {
      chai.request(app).post('/login').type('form')
        .send({username: 'User6', password: 'Password6'}).end((err, res) => {
          if (err) { assert.fail(err); }
          token.push(res.body.token);
          assert.ok(res.body.token);
          done();
        });
    });
  });

  describe('/campaign', () => {
    it('Responds with 401 for no token', (done) => {
      chai.request(app).get('/campaign').end((err, res) => {
          if (err) { assert.fail(err); }
          assert.equal(res.status, 401);
          done();
        });
    });

    it('Responds with error message for no token', (done) => {
      chai.request(app).get('/campaign').end((err, res) => {
          if (err) { assert.fail(err); }
          assert.equal(res.body.Error, 'No Token Provided');
          done();
        });
    });

    it('Responds with 401 for invalid token', (done) => {
      chai.request(app).get('/campaign')
        .set('Cookie', `token=${token[0]};`).end((err, res) => {
          if (err) { assert.fail(err); }
          assert.equal(res.status, 401);
          done();
        });
    });

    it('Responds with error message for invalid token', (done) => {
      chai.request(app).get('/campaign')
        .set('Cookie', `token=${token[0]};`).end((err, res) => {
          if (err) { assert.fail(err); }
          assert.equal(res.body.Error, 'Token Invalid/Expired');
          done();
        });
    });

    it('Responds with username & imgPath for valid token', (done) => {
      chai.request(app).get('/campaign')
        .set('Cookie', `token=${token[1]};`).end((err, res) => {
          if (err) { assert.fail(err); }
          assert.deepEqual(res.body, { username: 'User3', imgPath: 'Sports.jpg' });
          done();
        });
    });

    it('Responds with username & imgPath for valid token', (done) => {
      chai.request(app).get('/campaign')
        .set('Cookie', `token=${token[2]};`).end((err, res) => {
          if (err) { assert.fail(err); }
          assert.deepEqual(res.body, { username: 'User6', imgPath: 'SanFrancisco.jpg' });
          done();
        });
    });
  });
  describe('/campaign/all', () => {
    it('Responds with 401 for no token', (done) => {
      chai.request(app).get('/campaign/all').end((err, res) => {
          if (err) { assert.fail(err); }
          assert.equal(res.status, 401);
          done();
        });
    });

    it('Responds with error message for no token', (done) => {
      chai.request(app).get('/campaign/all').end((err, res) => {
          if (err) { assert.fail(err); }
          assert.equal(res.body.Error, 'No Token Provided');
          done();
        });
    });

    it('Responds with 401 for invalid token', (done) => {
      chai.request(app).get('/campaign/all')
        .set('Cookie', `token=${token[0]};`).end((err, res) => {
          if (err) { assert.fail(err); }
          assert.equal(res.status, 401);
          done();
        });
    });

    it('Responds with error message for invalid token', (done) => {
      chai.request(app).get('/campaign/all')
        .set('Cookie', `token=${token[0]};`).end((err, res) => {
          if (err) { assert.fail(err); }
          assert.equal(res.body.Error, 'Token Invalid/Expired');
          done();
        });
    });

    it('Responds with appropriate values for campaign', (done) => {
      chai.request(app).get('/campaign/all')
        .set('Cookie', `token=${token[1]};`).end((err, res) => {
          if (err) { assert.fail(err); }
          assert.deepEqual(res.body, campaigns);
          done();
        });
    });

    it('Responds with appropriate values for campaign', (done) => {
      chai.request(app).get('/campaign/all')
        .set('Cookie', `token=${token[1]};`).end((err, res) => {
          if (err) { assert.fail(err); }
          assert.deepEqual(res.body, campaigns);
          done();
        });
    });

    it('Updates campaign without failing', (done) => {
      [campaigns[0].campaignPriority, campaigns[2].campaignPriority] =
        [campaigns[0].campaignPriority, campaigns[2].campaignPriority];
      [campaigns[0], campaigns[2]] = [campaigns[0], campaigns[2]];

      chai.request(app).post('/campaign/all').type('form')
        .set('Cookie', `token=${token[1]};`)
        .send({campaigns: JSON.stringify(campaigns)}).end((err, res) => {
          if (err) { assert.fail(err); }
          assert.equal(res.status, 200);
          done();
        });
    });
  });
});
