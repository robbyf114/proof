const assert = require('assert');

const getCSVData = require('../db/csv');

const testCSVString = 'Header 1, Header 2, Header 3\n 4, 5, str1\n 3, 5, str2';

describe('getCSVData', () => {
  it('Smoke Test against given data set', () => {
    return getCSVData('assets/Proof_homework.csv',
      (e) => (assert.equal(e.length, 10))
    )
  });

  it('Testing Sample Data for specific row', () => {
    return getCSVData('assets/Proof_homework.csv',
      (e) => (assert.deepEqual(e[5], {
        'User ID': '6',
        'IP': '129.88.147.200',
        'Geo':'San Francisco',
        'Industry': 'Retail',
        'Company Size': '0-50'
      }))
    )
  });

  it('Testing generic csv string', () => {
    return getCSVData(testCSVString,
      (e) => (assert.deepEqual(e[0], {
        'Header 1': '4',
        'Header 2': '5',
        'Header 3': 'str1'
      }))
    )
  });
});
