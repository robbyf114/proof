# Proof

Proof Coding Test Solution
[Live Demo](https://proof-app.herokuapp.com/)

## Getting Started

### Prerequisites

This was built entirely in [Node.js](https://nodejs.org/en/), with an [express.js](https://expressjs.com/) backend, [React](https://reactjs.org/) frontend, and a [sqlite3](https://www.sqlite.org/index.html) db.

To install, you will need [Node.js](https://nodejs.org/en/) (preferably LTS), and [npm](https://www.npmjs.com/)

### Installing

Installation is fairly simple:

First, run:

```
npm install
```
To install all necessary dependencies.
### Running

From there on, you can just run

```
npm start
```
To start the application.

If you look at the [package.json](package.json) file, you will see that npm start is just running:
```
webpack -p; node app.js;
```
The webpack command builds the front end, and puts it in the /dist folder, and node app.js runs the web server.

## Running the Tests

To run the tests, simply run

```
npm test
```

Which will run the test suite.

### Test Breakdown

**app.test.js** This test suite tests the React Component, and makes sure everything is rendering correctly.

**backend.test.js** This test suite tests all of the api endpoints.

**csv.test.js** This test suite tests the csvToJSON method for reading in CSVs and returning JSON.

**db.test.js** This test suite tests the initDB method for populating the db.


## Deployment

Deployment with [GitLab](https://gitlab.com) is really cool! Their CI/CD tools are a rabbit hole of their own to go down [(which I did)](https://docs.gitlab.com/ee/ci/), but in short, I am deploying to a heroku container using the script defined in [.gitlab-ci.yml](.gitlab-ci.yml) which looks like:
```
image: node:latest

variables:
  # POSTGRES_USER: user
  # POSTGRES_PASSWORD: testing-password
  # POSTGRES_ENABLED: "true"
  # POSTGRES_DB: $CI_ENVIRONMENT_SLUG

stages:
  - test
  - production

test:
  # services:
  #   - postgres:latest
  # variables:
  #   POSTGRES_DB: test
  stage: test
  image: node:latest
  script:
    - npm install
    - npm test
  only:
    - master
  except:
    variables:
      - $TEST_DISABLED


production:
  type: deploy
  stage: production
  image: ruby:latest
  script:
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --app=proof-app --api-key=$HEROKU_API_KEY
  only:
    - master
```

## Afterthoughts

This was a really fun project! Thanks for the opportunity to work on this, and show you my skillset. I look forward to hearing from you guys!

-- Robby Fletcher
