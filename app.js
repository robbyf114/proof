// NPM Module Imports
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const express = require('express');
const jwt = require('jsonwebtoken');
const morgan = require('morgan');
const path = require('path');
const sqlite3 = require('sqlite3').verbose();

// Local Imports
const initDB = require('./db/db');

// Web server Set Up
const app = express();
const db = new sqlite3.Database('db.sqlite3');
const port = process.env.PORT || 3000;

// Run Script to initialize DB
initDB('assets/Proof_homework.csv');

// Express.js Middleware Set Up
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/dist'));
app.use(morgan('combined'));

// Login API Endpoint
app.post('/login', (req, res) => {
  // Check if credentials sent are valid, if so, send back token
  // If not, return 401 Error
  db.get('SELECT * FROM user u WHERE u.username = (?)', req.body.username, (err, user) => {
    if (!user || user.password != req.body.password) {
      res.status(401).json({ Error: 'Invalid Username or Password' });
    } else {
      console.log(req.body);
      res.json({ token: jwt.sign(req.body, 'Pr00f', { expiresIn: 120 * 60 }) });
    }
  });
});

// Campaign API Endpoint for user's campaign
app.get('/campaign', (req, res) => {
  if (!req.cookies.token) {
    res.status(401).json({ Error: 'No Token Provided' });
  } else {
  // Verify and decode the token to get User
    jwt.verify(req.cookies.token, 'Pr00f', (err, user) => {
      if (err) {
        res.status(401).json({ Error: 'Token Invalid/Expired' });
      } else {
        console.log(user);
        // Get the Highest Priority Campaign for this User
        db.get(`
          SELECT
            rnk.username username,
            c.campaignImg campaignImg
          FROM campaign c
          INNER JOIN (
              SELECT
                u.userID,
                MIN(u.username) username,
                MIN(c.campaignPriority) campaignPriority
              FROM user u
              INNER JOIN campaign c
                ON u.geoID = c.geoID
                  OR u.industryID = c.industryID
                  OR u.companySizeID = c.companySizeID
              WHERE u.username = (?)
              GROUP BY u.userID
            ) rnk
            ON c.campaignPriority = rnk.campaignPriority
        `, user.username, (err, campaign) => {
          // Send back the image path pertaining to the User's Campaign
          res.json({ username: campaign.username, imgPath: campaign.campaignImg });
        });
      }
    });
  }
});

// Campaign API Endpoint for all campaigns
app.get('/campaign/all', (req, res) => {
  if (!req.cookies.token) {
    res.status(401).json({ Error: 'No Token Provided' });
  } else {
  // Verify and decode the token to get User
    jwt.verify(req.cookies.token, 'Pr00f', (err, user) => {
      if (err) {
        res.status(401).json({ Error: 'Token Invalid/Expired' });
      } else {
        // Get all campaigns
        db.all(`
          SELECT
            c.campaignID,
            c.campaignImg,
            c.campaignPriority,
            g.geoDesc,
            i.industryDesc,
            cs.companySizeDesc
          FROM campaign c
          LEFT OUTER JOIN geo g
            ON c.geoID = g.geoID
          LEFT OUTER JOIN industry i
            ON c.industryID = i.industryID
          LEFT OUTER JOIN companySize cs
            ON c.companySizeID = cs.companySizeID
        `, (err, campaigns) => {
          if (err) { console.error(err); }
          res.json(campaigns);
        });
      }
    });
  }
})

app.post('/campaign/all', (req, res) => {
  if (!req.cookies.token) {
    res.status(401).json({ Error: 'No Token Provided' });
  } else {
  // Verify and decode the token to get User
    jwt.verify(req.cookies.token, 'Pr00f', (err, user) => {
      if (err) {
        res.status(401).json({ Error: 'Token Invalid/Expired' });
      } else {
        // console.log(JSON.parse(res.body.campaigns));
        db.serialize(() => {
          db.run('DELETE FROM campaign', (err) => {
            if (err) { console.error(err); }
          });
          JSON.parse(req.body.campaigns).map((e) => {
            db.run(`
              INSERT OR IGNORE INTO campaign (campaignImg, campaignPriority, geoID, industryID, companySizeID) VALUES (
                ($campaignImg),
                ($priority),
                (SELECT geoID FROM geo WHERE geoDesc = ($geoDesc)),
                (SELECT industryID FROM industry WHERE industryDesc = ($industryDesc)),
                (SELECT companySizeID FROM companySize WHERE companySizeDesc = ($companySizeDesc))
              );
            `, {
                $campaignImg: e.campaignImg,
                $priority: e.campaignPriority,
                $geoDesc: e.geoDesc,
                $industryDesc: e.industryDesc,
                $companySizeDesc: e.companySizeDesc
              }, (err) => {
                if (err) { console.error(err); }
              });
          });
        });
        res.json({ Result: 'Success!'});
      }
    });
  }
})

// Endpoint to send the React Frontend assets
app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname + '/dist', 'index.html'));
});

// Start Web Server
app.listen(port, () => console.log(`Proof Campaign App listening on port ${port}!`));

module.exports = app;
