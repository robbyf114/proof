const sqlite3 = require('sqlite3').verbose();
const getCSVData = require('./csv');

// Filter to pass to arrays to get only unique values.
const uniqueFilter = (e, i, a) => (a.indexOf(e) == i);

// Error handler callback for db operations.
const errorHandler = (err) => {
  if (err) console.log(err);
};


const initDB = (path, cb) => {
  // Load sqlite3 db.
  const db = new sqlite3.Database('db.sqlite3');

  // Function that takes either filepath to csv, or actual csv string
  // and converts it to an array of json objects.
  getCSVData(path, (data) => {
    // Using serialize to ensure these db statements are not called asynchronously.
    db.serialize(() => {

      // Table creation
      db.run(`
        CREATE TABLE IF NOT EXISTS user (
          userID INTEGER PRIMARY KEY,
          username TEXT,
          password TEXT,
          ipAddress TEXT,
          geoID INTEGER,
          industryID INTEGER,
          companySizeID INTEGER
        );
      `, errorHandler);

      // Table creation
      db.run(`
        CREATE TABLE IF NOT EXISTS geo (
          geoID INTEGER PRIMARY KEY,
          geoDesc TEXT UNIQUE
        )
      `, errorHandler);

      // Table creation
      db.run(`
        CREATE TABLE IF NOT EXISTS industry (
          industryID INTEGER PRIMARY KEY,
          industryDesc TEXT UNIQUE
        )
      `, errorHandler);

      // Table creation
      db.run(`
        CREATE TABLE IF NOT EXISTS companySize (
          companySizeID INTEGER PRIMARY KEY,
          companySizeDesc TEXT UNIQUE
        )
      `, errorHandler);

      // Table creation
      db.run(`
        CREATE TABLE IF NOT EXISTS campaign (
          campaignID INTEGER PRIMARY KEY,
          campaignImg TEXT,
          campaignPriority INTEGER UNIQUE,
          geoID INTEGER,
          industryID INTEGER,
          companySizeID INTEGER
        )
      `, errorHandler);

      // Insert Statement Prep
      const geosStmt = db.prepare('INSERT OR IGNORE INTO geo (geoDesc) VALUES (?)', errorHandler);
      const indsStmt = db.prepare('INSERT OR IGNORE INTO industry (industryDesc) VALUES (?);', errorHandler);
      const compSizesStmt = db.prepare('INSERT OR IGNORE INTO companySize (companySizeDesc) VALUES (?);', errorHandler);

      const usersStmt = db.prepare(`
        INSERT OR IGNORE INTO user (userID, username, password, ipAddress, geoID, industryID, companySizeID) VALUES (
          (?),
          (?),
          (?),
          (?),
          (SELECT geoID FROM geo WHERE geoDesc = (?)),
          (SELECT industryID FROM industry WHERE industryDesc = (?)),
          (SELECT companySizeID FROM companySize WHERE companySizeDesc = (?))
        );
      `, errorHandler);

      const campaignStmt = db.prepare(`
        INSERT OR IGNORE INTO campaign (campaignImg, campaignPriority, geoID, industryID, companySizeID) VALUES (
          ($campaignImg),
          ($priority),
          (SELECT geoID FROM geo WHERE geoDesc = ($geoDesc)),
          (SELECT industryID FROM industry WHERE industryDesc = ($industryDesc)),
          (SELECT companySizeID FROM companySize WHERE companySizeDesc = ($companySizeDesc))
        );
      `, errorHandler);

      // This takes the aray of objects, filters based on key in the objects,
      // filters for uniqueness, and then inserts them into the db.
      data.map((e) => (e['Geo']))
        .filter(uniqueFilter)
        .map((e) => (geosStmt.run(e, errorHandler)));

      data.map((e) => (e['Industry']))
        .filter(uniqueFilter)
        .map((e) => (indsStmt.run(e, errorHandler)));

      data.map((e) => (e['Company Size']))
        .filter(uniqueFilter)
        .map((e) => (compSizesStmt.run(e, errorHandler)));

       // Inserts each user into the users table
      data.map((e) => (usersStmt.run(e['User ID'], 'User' + e['User ID'],
        'Password' + e['User ID'], e['IP'], e['Geo'], e['Industry'],
        e['Company Size'], errorHandler)));

      // Pre-defined campaign priority
      // TODO: move this into a csv that is loaded in
      let campaigns = [
        {$priority: 1, $geoDesc: 'Austin', $campaignImg: 'Austin.jpg'},
        {$priority: 2, $geoDesc: 'San Francisco', $campaignImg: 'SanFrancisco.jpg'},
        {$priority: 3, $industryDesc: 'Software', $campaignImg: 'Software.jpg'},
        {$priority: 4, $industryDesc: 'Sports', $campaignImg: 'Sports.jpg'},
        {$priority: 5, $companySizeDesc: '0-50', $campaignImg: 'proof.png'},
        {$priority: 6, $companySizeDesc: '100-200', $campaignImg: 'smb.jpg'},
        {$priority: 7, $campaignImg: 'shrug.jpg'},
      ]

      // Insert campaigns into db
      campaigns.map((e) => campaignStmt.run(e, errorHandler));

      // Close out db insert statements
      geosStmt.finalize();
      indsStmt.finalize();
      compSizesStmt.finalize();
      usersStmt.finalize();
      campaignStmt.finalize();

    });

    db.close(cb);
  });
};

module.exports = initDB;
