const fs = require('fs');

// Function that reads in a file and passes it to a callback
const readFile = (filePath, cb) => {
  let res = '';
  fs.createReadStream(filePath)
    .on('data', (data) => {
      res = data.toString('utf-8');
    })
    .on('end', () => (cb(res)));
}

const csvToJSON = (csv, cb) => {
  // Split by line ending \r\n
  csv = csv.split('\r\n');
  // If that didn't split, split by \n
  if (csv.length == 1) csv = csv[0].split('\n');

  // Split each line by commas
  csv = csv.map((e) => {
    return e.split(',').map((e) => (e.trim()));
  });

  // Grab first row to use as headers
  let headers = csv.shift().filter((e) => (e != ''));

  // Converting from array into object
  csv = csv.map((e, i, a) => {
    let d = {};
    headers.forEach((he, hi) => {
      d[he] = e[hi];
    });
    return d;
  });

  // Filtering out empty rows
  csv = csv.filter((e) => (e[headers[0]] != ''));

  if (cb) {
    cb(csv);
  }
}

const getCSVData = (d, cb) => {
  // If it's a file, read in file, then convert to JSON
  if (d.slice(-4) == '.csv') {
    readFile(d, (csv) => (csvToJSON(csv, cb)));
  // If it's a string, just convert to JSON
  } else {
    csvToJSON(d, cb);
  }
}

module.exports = getCSVData;
